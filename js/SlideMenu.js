SlideMenu = (function(){
	var options;
	var holder;
	var el = {};
	var login = false;
	
	function init(hold, opt){
		holder = hold;
		options = opt || {};
		
		initUserCard();
		bind();
	}
	function initUserCard(){
		el.user_card = mui(".menu-user-card")[0];
//		var user = AV.User.current();
		el.login_btn = el.user_card.querySelector("#login");
		el.username =el.user_card.querySelector(".card-contact-name");
		el.photo = el.user_card.querySelector(".card-photo");
//		if(user){
//			Cloud.fetch(user);
//			setLogin(user);
//		}else{
//			setLogout(user);
//		}
		
//		App.BindTapEvent("login", function(){
//			if(login){
//				setLogout();
//				mui.toast("登出成功")
//			}
//			else{
//				var register_html = "../html/register.html";
//				App.openWin(register_html, {previous_win_id: window.current_win_id, current_win_id:register_html});
//				
//			}
//		})
		
	}
	function setLogin(user){
		console.log(JSON.stringify(user))
		el.photo.src = "../resource/photo/" +user.userPhoto;
		el.username.innerHTML = user.userName;
		console.log(el.username.innerHTML)
		login = true;
	}
	function setLogout(user){
		AV.User.logOut();
//		el.login_btn.style.display = "inline"
//		el.login_btn.innerHTML = "登陆"
		el.username.innerHTML = ""
		login = false;
	}
	
	function bind(){
		App.BindTapEvent("search", function(){
			close();
		});
		App.BindTapEvent("checkout", function(){
			setLogout();
			showRegView();
			close();
		});
		
		
	}
	function open(){holder.offCanvas('open');}
	function close(){holder.offCanvas('close');}
	
	return{
		init: init,
		open: open,
		close: close,
		setUser: setLogin,
		
	}
	
	
}())
