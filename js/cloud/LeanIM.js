LeanIM = (function(R){
	
	var my_id = "alex";
	
	function create(o, done){
		
		R.post("/classes/_Conversation", o, function(xhr, status){
			
			if(status == "success"){
				o.conv_id = xhr.objectId;
				done&& done(o)
			}

		})
	}
	
	function send(o, done){
		var params = {
			from_peer: o.from_peer || my_id,
			conv_id: o.conv_id,
			transient: false,
			message: o.message,
			no_sync: true,
			wait: false,
		}
		
		/*master key*/
		R.post("/rtm/messages", params, function(xhr, status){
			if(status == "success"){
				done&& done(o)
			}
			console.log(JSON.stringify(xhr));
			console.log(JSON.stringify(status))
		}, true)
	}
	
	function getConvLog(o , done){
		var params = {
			convid: o.conv_id,
//			max_ts: ,
			limit: o.limit || 100,
//			from: o.from,
			
		}
		R.get("/rtm/messages/logs", params, function(xhr, status){
			if(status == "success"){
				done&& done(o, xhr)
			}
//			console.log(JSON.stringify(xhr.length));
			console.log(JSON.stringify(o));
//			console.log(JSON.stringify(status))
			
		})
		
	}
	
	function getConvFrom(o, done){
		var params = {
			limit: o.limit || 100,
			from: o.from,
			
		}
		R.get("/rtm/messages/logs", params, function(xhr, status){
			if(status == "success"){
				done&& done(xhr)
			}
			console.log(JSON.stringify(xhr.length));
			console.log(JSON.stringify(xhr));
			console.log(JSON.stringify(status))
			
		})
	}
	function sendMsg(o){
		
		if(!o.conv_id){
			create(o, send);
		}
		else{
			send(o);
		}

	}
	
	
	
	
	
	
	
	
	return{
		create: create,
		send: send,
		getConv: getConvLog,
		getFrom: getConvFrom,
		
	}
	
	
	
}(REST))
